
import { NativeModules } from 'react-native';

const { FSFLocalNotifications } = NativeModules;

export default FSFLocalNotifications;
