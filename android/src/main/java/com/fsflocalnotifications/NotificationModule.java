
package com.fsflocalnotifications;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

public class NotificationModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;
  private final String CHANNEL_ID = "FSFUpdates";
  private int iconID;

  public NotificationModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "FSFLocalNotifications";
  }

  private void registerNotificationChannel() {
    CharSequence name = "FSF Updates";
    String description = "Important FSF Action Alerts and Updates";
    int importance = NotificationManager.IMPORTANCE_DEFAULT;
    NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
    channel.setDescription(description);

    NotificationManager notificationManager = reactContext.getSystemService(NotificationManager.class);
    notificationManager.createNotificationChannel(channel);
  }

  @ReactMethod
  public void initializeNotifications() {
    iconID = reactContext.getResources().getIdentifier("notification_icon", "drawable", reactContext.getPackageName());
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        registerNotificationChannel();
    }
  }

  @ReactMethod
  public void publishNotification(String title, String content, String link, int notificationId) {
    Intent notificationIntent = new Intent(Intent.ACTION_VIEW);
    notificationIntent.setData(Uri.parse(link));
    PendingIntent pending = PendingIntent.getActivity(reactContext, notificationId, notificationIntent, 0);

    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(reactContext, CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(content)
            .setSmallIcon(iconID)
            .setContentIntent(pending)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(reactContext);
    notificationManager.notify(notificationId, mBuilder.build());
  }
}
